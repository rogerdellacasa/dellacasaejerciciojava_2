package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicioo11 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		char letra;
		
		System.out.println("Ingresar letra ---->");
		letra = scan.next().charAt(0);
				
		if(letra=='a' || letra=='e' || letra=='i' || letra=='o' || letra=='u' ){
			System.out.println("La letra es una vocal");
		}
		
		else {
			System.out.println("La letra es una consonante");
		}
				
		scan.close();
		
	}

}
