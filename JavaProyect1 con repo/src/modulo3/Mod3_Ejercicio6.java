package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicio6 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		int a�os; 
		
		System.out.println("Ingresar a�os de estudio ");
		a�os = scan.nextInt();
		
		if(a�os ==0) {
			System.out.println("Curso: Jardin de infantes");
		}
		else if (a�os<=6) {
			System.out.println("Curso: Primaria");
		}
		else if (a�os<=12) {
			System.out.println("Curso: Secundaria");
		}
		else {
			System.out.println("Valor incorrecto");
		}
		
		scan.close();
		
		
	}

}
