package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicioo12 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
	
	int numero;
	
	System.out.println("Ingresar numero");
	numero = scan.nextInt();
	
	if(numero>=1 && numero<=12) {
	System.out.println("Numero pertenece a la primer docena");
	}
	else if(numero>=13 && numero<=24) {
		System.out.println("Numero pertenece a la segunda docena");
		}
	else if(numero>=24 && numero<=36) {
		System.out.println("Numero pertenece a la tercer docena");
		}
	else {
		System.out.println("El numero " +numero+ " esta fuera de rango");
	}
	
	scan.close();
	
	
	}

}
