package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicio8 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		String jugador1;
		String jugador2;
	
	System.out.println("Ingresar Piedra (1), Papel(2), Tijera (3)");
	System.out.println("Jugador 1 ---->");
	jugador1 = scan.nextLine();
	System.out.println("Jugador 2");
    jugador2 = scan.nextLine();
    
    if(jugador1.equals("1")) {
    	if(jugador2.equals("2")) {
    		System.out.println("Gana Jugador 2");
    	}
    	else if(jugador2.equals("3")) {
    		System.out.println("Gana Jugador 1");
    	}
    	else {
    		System.out.println("Empate");
    	}
    }
    if(jugador1.equals("2")) {
    	if(jugador2.equals("1")) {
    		System.out.println("Gana Jugador 1");
    	}
    	else if(jugador2.equals("3")) {
    		System.out.println("Gana Jugador 2");
    	}
    	else {
    		System.out.println("Empate");
    	}
    }
    if(jugador1.equals("3")) {
    	if(jugador2.equals("1")) {
    		System.out.println("Gana Jugador 2");
    	}
    	else if(jugador2.equals("2")) {
    		System.out.println("Gana Jugador 1");
    	}
    	else {
    		System.out.println("Empate");
    	}
    }
    	
    	scan.close(); 	
	
	}

}
