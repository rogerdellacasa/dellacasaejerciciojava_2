package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicio3 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese el mes ");
		String mes = scan.next();
		
		if (mes.equals("Febrero")||mes.equals("febrero")) {
			System.out.println(mes + "tiene 28 dias");
				}
		else if (mes.equals("Abril")||mes.equals("abril")) {
				System.out.println(mes + "tiene 30 dias");
		}
		else if (mes.equals("Junio")||mes.equals("junio")) {
			System.out.println(mes + "tiene 30 dias");
		}
		else if (mes.equals("Septiembre")||mes.equals("septiembre")) {
			System.out.println(mes + "tiene 30 dias");
		}
		else if (mes.equals("Noviembre")||mes.equals("noviembre")) {
			System.out.println(mes + "tiene 30 dias");
		}
		else if (mes.equals("Enero")||mes.equals("enero")) {
			System.out.println(mes + "tiene 31 dias");
		}
		else if (mes.equals("Marzo")||mes.equals("marzo")) {
			System.out.println(mes + "tiene 31 dias");
		}
		else if (mes.equals("Mayo")||mes.equals("mayo")) {
			System.out.println(mes + "tiene 31 dias");
		}
		else if (mes.equals("Julio")||mes.equals("julio")) {
			System.out.println(mes + "tiene 31 dias");
		}
		else if (mes.equals("Agosto")||mes.equals("agosto")) {
			System.out.println(mes + "tiene 31 dias");
		}
		else if (mes.equals("Octubre")||mes.equals("octubre")) {
			System.out.println(mes + "tiene 31 dias");
		}
		else if (mes.equals("Diciembre")||mes.equals("diciembre")) {
			System.out.println(mes + "tiene 31 dias");
		}
		else {
			System.out.println("Mes incorrecto");
		}
		
		scan.close();
	}

}
