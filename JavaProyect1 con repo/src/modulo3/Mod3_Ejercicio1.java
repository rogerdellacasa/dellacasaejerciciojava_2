package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicio1 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		double nota1, nota2, nota3, promedio;
		
		System.out.println("ingrese nota 1: ");
		nota1 = scan.nextDouble();
		System.out.println("Ingrese nota 2:");
		nota2 = scan.nextDouble();
		System.out.println("Ingrese nota 3:");
		nota3 = scan.nextDouble();
		
		promedio = (nota1+nota2+nota3)/3;
		if(promedio>7) {
			System.out.println("Aprobaste con ---->" +promedio);
		}
		else {
			System.out.println("Desaprobaste con ---->" +promedio);
		}
		
		scan.close();
	}

}
