package modulo2;

public class Mod2_Ejercicio3 {

	public static void main(String[] args) {
		char div = 'c';
        byte gol = 2;
        int cancha = 70000;
        float promgol = 1.2F;
 
        System.out.println("Division:\t\t"+ div);
        System.out.println("Cantidad de goles:\t"+ gol);
        System.out.println("Capacidad de la cancah:\t"+ cancha);
        System.out.println("Promedio de goles:\t"+ promgol);

	}

}
