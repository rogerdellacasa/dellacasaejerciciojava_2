package modulo1;

public class ejercicio3 {

	public static void main(String[] args) {
		System.out.println("\tTecla de escape\t\t\tSignificado");
		System.out.println("\n\t\\n \t\t\t\tsignifica nueva l�nea");
		System.out.println("\n\t\\t \t\t\t\tsignifica un tab de espacio");
		System.out.println("\n\t\\\" \t\t\tes para poner � (comillas dobles) dentro del texto por ejemplo �Belencita�");
		
		System.out.println("\n\t\\\\ \t\t\t\tse utiliza para escribir la \\\\ dentro del texto, por ejemplo \\algo\\");
		System.out.println("\n\t\\' \t\t\t\tse utiliza para las �(comilla simple) para escribir por ejemplo �Princesita�");
	}
}
