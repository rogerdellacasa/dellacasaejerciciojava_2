package modelo.componentes.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.AssertionFailedError;
import modelo.componentes.Inductor;

public class InductorTest {
	
	//defino los objetos que voy a utlizar para esl testeo
	//lote de pruebass
	Inductor inductorVacio ; //1- definiccion
	Inductor inductorConParametros ; //1- definiccion
	List<Inductor> inductoresList;
	Set<Inductor>  inductoresSet;
	@Before
	public void setUp() throws Exception {
		inductorVacio = new Inductor();
		inductorConParametros = new Inductor("L1", 0.003f, 6000);
		
		//creo mi lote de pruebas con una List
		inductoresList = new ArrayList<>(); //ordered
		inductoresList.add(new Inductor("indu1",0.1f, 100));
		inductoresList.add(new Inductor("indu2",0.2f, 200));
		inductoresList.add(new Inductor("indu3",0.3f, 300));
		inductoresList.add(new Inductor("indu4",0.4f, 400));
		inductoresList.add(new Inductor("indu5",0.5f, 500));
		//cre mi lote de pruebas con una Set
		inductoresSet = new HashSet<>(); //ordered
		inductoresSet.add(new Inductor("indu1",0.1f, 100));
		inductoresSet.add(new Inductor("indu2",0.2f, 200));
		inductoresSet.add(new Inductor("indu3",0.3f, 300));
		inductoresSet.add(new Inductor("indu4",0.4f, 400));
		inductoresSet.add(new Inductor("indu5",0.5f, 500));
		
		
	}

	@After
	public void tearDown() throws Exception {
		inductorVacio 			= null;
		inductorConParametros 	= null;
				
		inductoresList=null;
		inductoresSet =null;
	}
// ******** Contro del constructor vacio
	@Test
	public void testInductorVacioNombre() {
		assertEquals("Ldef", inductorVacio.getNombre());
		
	}
	@Test
	public void testInductorVacioUnidad() {
		assertEquals("Hy", inductorVacio.getUnidad());		
	}
	
	@Test
	public void testCalcularImpedanciaInductorVacio() {
		assertEquals(6.28, inductorVacio.calcularImpedancia(), 0.01);		
	}
	//de estos 25 como  minimo
//*************fin de test de constructor Vacio ******
	//TODO Alumnos hay que agregar los testeos del constructor con parametros
	
	@Test
	public void testInductorEquals() {
		Inductor otro = new Inductor();		
		assertTrue(inductorVacio.equals(otro));		
	}
	@Test
	public void testEqalsEmListTRUE(){
		Inductor otro = new Inductor("indu2",0.2f, 200);
		assertTrue(inductoresList.contains(otro));
	}
	@Test
	public void testEqualsEnListFALSE(){
		assertFalse(inductoresList.contains(inductorVacio));
	}
	@Test
	public void testEqualsSetTrue(){
		
		assertFalse( inductoresSet.add(new Inductor("indu3",0.3f, 300)));
		
	}

}
