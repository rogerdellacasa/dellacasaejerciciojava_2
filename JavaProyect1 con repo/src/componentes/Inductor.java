/**
 * 
 */
package componentes;

/**
 * Esta clase va a agrupar las caracteristicas de un inductor y su comportamiento
 * @author Gabriel
 *
 */


public class Inductor {	
	//atributos
	private String nombre;
	private float valor;
	private String unidad;
	private int frecuencia;
	
	//constructores
	
	public Inductor(){
		//se le agregan valor por defecto
		nombre ="Ldef";
		valor = 0.001f;
		unidad = "Hy";
		frecuencia = 1000;
	}
	public Inductor(String pNom, float pValor, int pFcia){
		unidad = "Hy";
		nombre =pNom;
		valor = pValor;
		this.frecuencia = pFcia;
		
	}
	
	//accessors o getter y setter
	
	public void setNombre(String pNom){
		nombre = pNom;
	}
	public String getNombre(){
		return nombre;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public String getUnidad() {
		return unidad;
	}

	public int getFrecuencia() {
		return frecuencia;
	}
	public void setFrecuencia(int frecuencia) {
		this.frecuencia = frecuencia;
	}
	//nedodos de negocio
	public float calcularImpedancia(){
		return (float)(2*Math.PI* this.frecuencia * this.valor);
	}
	//metods especiales, y felices
   public boolean equals(Object obj){
	   boolean bln = false;
	  
	   if(obj!=null && obj instanceof Inductor){
		   Inductor indu = (Inductor)obj;
		   bln = indu.getNombre().equals(nombre)    &&
				 indu.getFrecuencia() == frecuencia &&
				 indu.valor == valor;
	   }
	   return bln;
	   
   }
   public int hashCode(){
	   return nombre.hashCode() + frecuencia + (int) valor;
   }
   public String toString(){
	   return "nombre:" 		+ nombre 	+
			   ",valor:" 		+ valor 	+
			   ",unidad:" 		+ unidad 	+
			   ", frecuencia:" 	+ frecuencia;
   }
	
	
	
}
